
# Development base path
BASE_PATH = ''

#PythonEverywhere base path
#BASE_PATH = 'mysite/'

UPLOAD_FOLDER = 'uploads_fits'
FOLDER_TO_CLEAR = ['static/fits_png_tmp', 'static/sunpy_tmp', 'uploads_fits']
ALLOWED_EXTENSIONS = {'fit', 'fits', 'png'}



#http://127.0.0.1:5000/clean_all_files_now?cleankey=hPadi6uQVjHCfzWqpqZ7qGENTa53V8N
#mat2857754.pythonanywhere.com
#autodiscover.checksun	adsredir.ionos.info