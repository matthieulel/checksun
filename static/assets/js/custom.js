var cropperFits;
var cropperDragMode = false;
// Default rotation value
var rotationValue = 5;
var sdo_img = [];
var sdoSelectedButton;
var pageLanguage = $('html').attr("lang");
var flipperH = false;
var flipperV = false;
var timer = null;
//Time limite before to abort the ajax request, used if the request is too long
var ajaxLimitTime = 150000;

// when document is ready
$(function() {
    $('#file_mark').hide()
    hide_loader()
    hide_loader_error()

    //set date of today on date picker
    //Format : 2021-09-01T00:00
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth() + 1)).slice(-2);
    let today = now.getFullYear()+"-"+(month)+"-"+(day)+"T00:00" ;
    $('#start_date').val(today);

});

// Event triggered everytime the modal is closed
$('#modal-settings').on('modal:close',function(event, modal) {
    //set rotation value to the actual saved one
    $('#rotation').val(rotationValue);
});

function hide_loader() {
    $('#loader').hide();
}

function hide_loader_error() {
    $('#loader_error').hide();
}

function set_file_format(btn_id){
    if (btn_id == 'btn_png') {
        $('#btn_png').addClass('is_selected_btn')
        $('#btn_fits').removeClass('is_selected_btn')
        $('#selectedFile').attr('accept', '.png')

        verif_date_available();
    } else if (btn_id == 'btn_fits') {
        //alert(btn_id + 'ok');
        $('#btn_fits').addClass('is_selected_btn')
        $('#btn_png').removeClass('is_selected_btn')
        $('#selectedFile').attr('accept', '.fits ,.fit')
    } else {
        alert( pageLanguage =='fr' ? 
        'Veuillez sélectionner un format de fichier' : 
        'You must choose a file format'
        )
    }
}

// Vérification de la date -> True si inférieur à la date + UTC actuelle -1H en attendant les données SDO
function verif_date_available(){
    let date_selected = $('#start_date').val()
    let now = new Date();

    var utc_date = new Date();
    let utcday = ("0" + utc_date.getUTCDate()).slice(-2);
    let utcmonth = ("0" + (now.getUTCMonth() + 1)).slice(-2);

    if((utc_date.getUTCHours() - 1) > 0) {
        if (date_selected >= (utc_date.getUTCFullYear() + "-" + (utcmonth) + "-" + (utcday) + "T" + (utc_date.getUTCHours() - 1) + ":" + (utc_date.getUTCMinutes()))) {
            alert( pageLanguage =='fr' ? 
            "La date et l'heure sélectionnées sont supérieures à la date et l'heure actuelles ou trop proches. Veuillez sélectionner une nouvelle date." :
            "The selected date and time are greater than or too close to the current date and time. Please select a new date."   
            )
            return false;
        } else {
            return true;
        }
    }
}


// show the file upload mark
function show_file_checker(){
    $('#file_mark').show()
}

function ajax_upload(){
    disableLaunchButton()

    // Verification if date is < now
    if(!verif_date_available()){
        enableLaunchButton()
        return;
    }

    var form = new FormData(); 

    // Checking if a file has been selected
    if ((($('#selectedFile')[0].files.length)) == 0) {
        enableLaunchButton()
        alert( pageLanguage =='fr' ? "Vous n'avez pas sélectionné de fichier !" : "You must select a file !")
    } else {
        //Clearing phase
        clearGallery()
        hide_loader_error()

        var start_date = $('#start_date').val()
        var files = $('#selectedFile')[0].files
        // Based on file.name instead of file.type because the browser doesn't detect the .fits image type
        // Check at -> console.log(files)
        var file_type = files[0].name.split(".")
        var data_to_add = [
            ['file','start_date','filetype','lang'],
            [files[0],start_date,file_type[1],pageLanguage]
        ]

        $('#loader').show();
        launch_text();
        go_result_zone("loader")

        //Append all data to the form
        for (let i = 0; i <  data_to_add[0].length; i++) {
            form.append(data_to_add[0][i],data_to_add[1][i]);
        }

        var xhr = $.ajax({
            url: '/ajax',
            type: 'post',
            data: form,
            contentType: false,
            processData: false,
            success: function(response){
                if(response.length != 0) {
                    if (response[2]){ // if sunpy return data => bool true
                        hide_loader()
                        enableLaunchButton()

                        sdo_img = response[0]
                        let fits = response[1]

                        //Display the gallery element
                        $('#gallery').show()
                        //Change the src attribute by the path of the images
                        $('#fits').attr("src","static/" + fits );
                        // By default we display the first img of the array which is the first wavelength
                        $('#aia').attr("src","static/" + sdo_img[0] );

                        var fits_path = $('#fits')[0];
                        
                        //Cropper instance
                        cropperFits = new Cropper(fits_path, {
                         //   aspectRatio: 4 / 3,
                            viewMode : 0,
                            zoomOnWheel: false
                            },
                        );

                        go_result_zone("gallery")
                    } else {
                        enableLaunchButton()
                        hide_loader()
                        go_result_zone("loader_error")
                        $('#loader_img').attr('src', 'static/images/error.png');
                        $('#loader_error').show()
                        change_text('loader_error_text' , pageLanguage =='fr' ?
                        "Oups... Impossible de récupérer des données pour la date sélectionnée - Veuillez saisir une autre date, relancer la recherche, ou réessayer plus tard." :
                        "Oops... Unable to retrieve data for the selected date - Please enter another date, search again, or try again later."
                        )
                    }
                } else {
                    enableLaunchButton()
                    hide_loader()
                    alert(pageLanguage =='fr' ? 'Fichier non uploadé' : 'File not uploaded');
                }
            },
            error: function(response){
                enableLaunchButton()
                hide_loader()
                if (response.status == 403){
                    //Using setTimeout to display the alert after hiding the loader, time set on 50ms because the error response time is actually ~~10ms
                    setTimeout(alert.bind(null,response.responseJSON,50))
                }
            }
        });
    }   

    var ajaxTimeout = setTimeout(function() {
        xhr.abort();
        if($('#gallery').css('display') == 'none') {
            enableLaunchButton()
            hide_loader()
            go_result_zone("loader_error")
            $('#loader_img').attr('src', 'static/images/error.png');
            $('#loader_error').show()
            change_text('loader_error_text' , pageLanguage =='fr' ?
            "Oups... Le téléchargement depuis le server distant prend trop de temps, il se peut que ce dernier soit en maintenance... - Veuillez saisir une autre date, relancer la recherche, ou réessayer plus tard." :
            "Oops... The download from the remote server is taking too long, it may be under maintenance... - Please enter another date, search again, or try again later."
            )
        }
    }, ajaxLimitTime) 
}

function go_result_zone(idElement){
    let result_tag = $('#' + idElement);
    $('html,body').animate({scrollTop: result_tag.offset().top},'slow');
}

function change_text(id , text){
    $('#' + id ).text(text)
}

function launch_text() {
    setTimeout(function() {
        change_text( 'loading_text' , pageLanguage =='fr' ? 'Chargement du fichier...' : 'Loading file...' )
        setTimeout(function() {
            change_text( 'loading_text' , pageLanguage =='fr' ? 'Chargement des données SDO...' : 'Loading SDO data...' )
            setTimeout(function() {
                change_text('loading_text',  pageLanguage =='fr' ? 'Préparation des données pour affichage...' : 'Preparing data for display...')
            }, 6000);
        }, 6000);
    }, 6000);
}

function DownloadCroppedImage(cropper){
    var croppedimage = cropper.getCroppedCanvas().toDataURL("image/png");
    var link = document.createElement("a");
    link.download = "Crop_image";
    link.href = croppedimage;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}

function EnableDragMode(){
    if (!cropperDragMode){
        cropperFits.setDragMode('move')
        cropperDragMode = true;
        $('#move-action-fits').attr("title","Crop image")
        $('#move-icon-fits').attr("class","fas fa-crop-alt")
    } else {
        cropperFits.setDragMode('crop')
        cropperDragMode = false;
        $('#move-action-fits').attr("title", pageLanguage =='fr' ? "Déplacer l'image" : "Move image")
        $('#move-icon-fits').attr("class","fas fa-arrows-alt")
    }
}

function changeDisplayedImg(index, id){
    $('#aia').attr('src', 'static/' + sdo_img[index])
    changeSdoSelectedButton(id)
}

function changeSdoSelectedButton(id){
    switch (id) {
        case 'aia193':
            $('#aia193').addClass('is_selected_btn')
            $('#aia304').removeClass('is_selected_btn')
            $('#aia1700').removeClass('is_selected_btn')
            $('#hmimg').removeClass('is_selected_btn')
            break;
        case 'aia304':
            $('#aia304').addClass('is_selected_btn')
            $('#aia193').removeClass('is_selected_btn')
            $('#aia1700').removeClass('is_selected_btn')
            $('#hmimg').removeClass('is_selected_btn')
            break;
        case 'aia1700':
            $('#aia1700').addClass('is_selected_btn')
            $('#aia304').removeClass('is_selected_btn')
            $('#aia193').removeClass('is_selected_btn')
            $('#hmimg').removeClass('is_selected_btn')
            break;
        case 'hmimg':
            $('#hmimg').addClass('is_selected_btn')
            $('#aia304').removeClass('is_selected_btn')
            $('#aia1700').removeClass('is_selected_btn')
            $('#aia193').removeClass('is_selected_btn')
            break;
        default: $('#aia193').addClass('is_selected_btn')
            break;
    }
}

function applyRotation(rotationDegree){
    rotationValue = rotationDegree
    $.modal.close()
}

function imageFlipH(){
    if ( flipperH == false ) {
        cropperFits.scaleX(-1)
        flipperH = true
    } else {
        cropperFits.scaleX(1)
        flipperH = false
    }
}

function imageFlipV(){
    if ( flipperV == false ) {
        cropperFits.scaleY(-1)
        flipperV = true
    } else {
        cropperFits.scaleY(1)
        flipperV = false
    }
}

function clearGallery() {
    // If cropper exists, destroy it before to create a new one
    if (cropperFits) {
        cropperFits.destroy()
    }

    $('#gallery').hide()
}

function displaySnackbar(id) {
    // If timer's on , reset it 
    if (timer != null) {
        clearTimeout(timer)
        timer = null;
    }

    changeSnackBarText(id)

    $('#snackbar').addClass("show")

    timer = setTimeout(function(){
        $('#snackbar').removeClass("show")
        $('#snackbar').addClass("fadeout")
        setTimeout(function(){
            $('#snackbar').attr("class","")
        },480)
    }, 10000)
}

function changeSnackBarText(id){
    switch (id) {
        case 'aia193':
            $('#snackbar-text').text( pageLanguage === "fr" ?
            "Lumière émise par le Fer-12 et Fer-24 (Fe XII , XXIV) - Température de 1 200 000 K et 20 000 000 K. La région concerne ici la couronne solaire pour le premier et une partie beaucoup plus chaude du plasma lors d'une éruption solaire. (Plus d'informations sur : https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" :
            "Light emitted by Iron-12 and Iron-24 (Fe XII, XXIV) - Temperature from 1,200,000 K to 20,000,000 K. The area here concerns the solar corona for the first and a much hotter part of the plasma during the solar flare. (More information on: https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)"
            )
            break;
        case 'aia304':
            $('#snackbar-text').text( pageLanguage === "fr" ?
            "Lumière émise par l'hélium-2 (He II) - Température vers 50 000 K. La lumière est émise de la chromosphère et de la région de transition. (Plus d'informations sur : https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" :
            "Light emitted by helium-2 (He II) - Temperature around 50,000 K. Light is emitted from the chromosphere and the transition region. (More information on: https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)"
            )
            break;
        case 'aia1700':
            $('#snackbar-text').text( pageLanguage === "fr" ?
            "Lumière émise par le continuum - Température vers 5000 K. La lumière est émise de la chromosphère et qui est typiquement colorée en rose granuleux. (Plus d'informations sur : https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" :
            "Light emitted by the continuum - Temperature around 5000 K. The light is emitted from the chromosphere and is typically colored granular pink. (More information on: https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" 
            )
            break;
        case 'hmimg':
            $('#snackbar-text').text( pageLanguage === "fr" ?
            "Les magnétogrammes montrent des cartes du champ magnétique à la surface du soleil, le noir montrant les lignes de champ magnétique s'éloignant de la Terre et le blanc les lignes de champ magnétique se rapprochant de la Terre. (Plus d'informations sur : https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" :
            "Magnetograms show maps of the magnetic field on the sun’s surface, with black showing magnetic field lines pointing away from Earth, and white showing magnetic field lines coming toward Earth. (More information on: https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" 
            )
            break;
        default:             
            $('#snackbar-text').text( pageLanguage === "fr" ?
            "Lumière émise par le Fer-12 et Fer-24 (Fe XII , XXIV) - Température de 1 200 000 K et 20 000 000 K. La région concerne ici la couronne solaire pour le premier et une partie beaucoup plus chaude du plasma lors d'une éruption solaire. (Plus d'informations sur : https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)" :
            "Light emitted by Iron-12 and Iron-24 (Fe XII, XXIV) - Temperature from 1,200,000 K to 20,000,000 K. The area here concerns the solar corona for the first and a much hotter part of the plasma during the solar flare. (More information on: https://en.wikipedia.org/wiki/Solar_Dynamics_Observatory)"
            )
            break;
    }
}

function enableLaunchButton(){
    $('#launch').removeAttr("disabled")
}

function disableLaunchButton(){
    $('#launch').prop('disabled', true)
}

function closePopup (){
    if (timer != null) {
        clearTimeout(timer)
        timer = null;
    }

    $('#snackbar').removeClass("show")
    $('#snackbar').addClass("fadeout")
    $('#snackbar').attr("class","")
}

function disableCrop(){
    if (($(".cropper-container").children().eq(2).is(":hidden"))) {
        $('.cropper-container').children().eq(2).show();
        $('.cropper-modal').css('opacity','0.5');
        enableDownloadButton()
    } else {
        $('.cropper-container').children().eq(2).hide();
        $('.cropper-modal').css('opacity','0');
        disableDownloadButton()
    }
}

function enableDownloadButton(){
    $('#downloadButton').removeAttr("disabled")
    $('#downloadButton').css('opacity','1');
}

function disableDownloadButton(){
    $('#downloadButton').prop('disabled', true)
    $('#downloadButton').css('opacity','0.5');
}