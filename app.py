from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import os, shutil
import sunpy_utils as sp
from datetime import datetime
from flask import jsonify
from astropy.io import fits
import matplotlib.pyplot as plt
from PIL import Image
import PIL


# start flask app
app = Flask(__name__)
app.config.from_object('config')


def is_allowed_file(filetype):
    """
    Verification of filetype extension post in allowed list

            Parameters:
                    filetype (String): The type of the file

            Return: 
                    Boolean : -> True if filetype is in allowed list
                              -> False if filetype isn't in allowed list
    """
    return '.' in filetype and \
           filetype.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']


def fits_to_png(fits_filename):
    """
    Converting .fits into .png

            Parameters:
                    fits_filename (String): Name of a .fits file

            Return: 
                    fits_png_tmp/XXXXXXXXX.png (String) : File's name converted into a .png            
    """
    hdul = fits.open(app.config['BASE_PATH'] + os.path.join(app.config['UPLOAD_FOLDER'] + '/' + fits_filename))
    app.logger.info('Open file in fits to png in progress...')
    image_data = hdul[0].data
    fig_fits = plt.figure(figsize=(16, 9))
    plt.imshow(image_data, cmap='gray')
    base_fits_filename = fits_filename.split('.fits')[0]
    fig_fits.savefig(app.config['BASE_PATH'] + 'static/fits_png_tmp/' + base_fits_filename + '.png')
    app.logger.info('Fist to PNG done.')
    return 'fits_png_tmp/'+base_fits_filename+'.png'


def resizing_image(img_p, timestamp):
    fixed_width = 1024
    image = Image.open(img_p)
    width_percent = (fixed_width / float(image.size[0]))
    height_size = int((float(image.size[1]) * float(width_percent)))
    image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
    image.save('static/dataset_ia/' + str(timestamp) + "_halpha.png")

# ============================ ROUTES ============================ #


# EN Route
@app.route('/en')
def home_en():
    # Home here
    return render_template('index_en.html')


# FR Route
@app.route('/')
def home():
    # Home here
    return render_template('index.html')


@app.route('/ajax', methods=['GET', 'POST'])
def upload_ajax():
        """
        This function treats the request received as a POST method at the /ajax route
        It consists in few phases :
        -> A validation phase to verify if the method used is POST method,
           and then verifying if all the data we need are corrects
        -> A saving part to save the uploaded file in a folder and also convert the .fits into .png
        -> The searching phase which use sunpy library to fetch SDO pictures depending on the data sent in the request

        return :
                HTTP Error 403 : If a data isn't valid
                OR
                Json data format : aia_png_path (str) -> first element of the fetched pictures list from sunpy
                                   png_path (str) ->  Path of the png file to compare with
                                   is_aia_png_found (str) -> second element of the fetched SDO pictures
        """
        if request.method != 'POST':
            return
        # ------------------ Validation Phase -------------------

        # ----> filetype form data verification
        filetype = "." + request.form["filetype"]
        if not is_allowed_file(filetype):
            return jsonify('Type de fichier non pris en charge'), 403

        # ----> date form data verification
        if (request.form['start_date'] is None) or (request.form['start_date'] == ''):
            app.logger.info('Date form error')
            # return template with error message -> watch if error blok
            return jsonify('Date error'), 403

        # ----> file existence form data verification
        # file upload form verification
        # check if the post request has the file part
        if 'file' not in request.files:
            app.logger.info('No file part')
            # return template with error message -> watch if error block
            return jsonify('No file part'), 403
        # if the file is uploaded by user -> recuperation of file in "file"
        file = request.files['file']

        # ----> filename form data verification
        # if user does not select file, browser also
        # submit an empty part without filename
        length_name = len(file.filename)
        app.logger.info('Lengthname : ' + str(length_name))
        if file.filename == '':
            app.logger.error('No selected file')
            return jsonify('No file selected'), 403

        # -------------------------------------------

        start_date = request.form['start_date']
        date_tab = start_date.split('T')
        start_date = date_tab[0]
        start_time = date_tab[1]
        lang = request.form['lang']
        is_data_found = True
        # aia_wavelength = request.form['wavelength']

        # user data log
        app.logger.info(f'User date informations : {start_date} - {start_time}')
        app.logger.info(f'User language informations : {lang}')

        # -------------- Saving file phase ----------------------

        # initiate png_path
        png_path = ''

        # Save file in uploads_fits folder only if all data are OK.
        timestamp_tag = (int(datetime.utcnow().timestamp()))
        filename = secure_filename(file.filename)
        file_rename = str(timestamp_tag) + '_' + filename
        file.save(os.path.join(app.config['BASE_PATH'] + app.config['UPLOAD_FOLDER'], file_rename))

        if filetype in ['.fits', '.fit']:
            app.logger.info('work in progress')
            png_path = fits_to_png(file_rename)

        elif filetype == '.png':
            shutil.copyfile(app.config['BASE_PATH'] + os.path.join(app.config['UPLOAD_FOLDER'] + '/' + file_rename), app.config['BASE_PATH'] + 'static/fits_png_tmp/' + file_rename)
            png_path = 'fits_png_tmp/' + file_rename

        # Resize image for AI
        # image_to_resize = "static/" + png_path
        # resizingImage(image_to_resize, timestamp_tag)
        moment = start_date + ' ' + start_time

        # ------------ Data searching phase -------------

        # sunpy process
        aia_start, aia_stop = sp.dpicker_to_sunpy_format(moment, 10)
        # print(aia_start, aia_stop)
        sdo_png_path_list, is_data_found = sp.get_sdo_img(aia_start, aia_stop, lang)
        # is_aia_data_found = sdo_search[1]
        app.logger.info(f'Sunpy return : {sdo_png_path_list}')

        # render data
        return jsonify(sdo_png_path_list, png_path, is_data_found)


# ------------------ For development mode ---------------------#
if __name__ == '__main__':
    #app.run(debug=True)
    app.run(debug=True, host='0.0.0.0')
