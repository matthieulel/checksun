import logging
from datetime import datetime, timedelta
from sunpy.net import Fido, attrs as a
import sunpy.map
import astropy.units as u
import matplotlib.pyplot as plt


def get_aia_img(start_date_for_request: datetime, stop_date_for_request: datetime):
    # Initializing empty array
    dl = []

    # Searching AIA images
    aia_results = Fido.search(a.Time(start_date_for_request, stop_date_for_request), a.Instrument.aia)

    # If no result found, return false
    if aia_results.file_num == 0:
        logging.info("Pas de résultat AIA")
        # Returning False if no data found and the empty array
        return False, dl

    # Storing results by Wavelength
    aia_193 = aia_results[0][aia_results[0]["Wavelength"][:, 0] == 193 * u.AA]
    aia_304 = aia_results[0][aia_results[0]["Wavelength"][:, 0] == 304 * u.AA]
    aia_1700 = aia_results[0][aia_results[0]["Wavelength"][:, 0] == 1700 * u.AA]

    # Downloading first element of each Wavelength
    dl = Fido.fetch(aia_193[0], aia_304[0], aia_1700[0], path='uploads_fits')

    # Returning True if data found and the array of fetched images path
    return True, dl


def get_hmi_img(start_date_for_request: datetime, stop_date_for_request: datetime):
    # Initializing empty array
    dl = []

    # Searching HMI images
    hmi_results = Fido.search(a.Time(start_date_for_request, stop_date_for_request), a.Instrument.hmi)
    
    # If no result found, return false
    if hmi_results.file_num == 0:
        # Returning the empty array
        return False, dl

    # Storing results with magnetic field
    hmi_mg = hmi_results[0][hmi_results[0]["Physobs"] == "LOS_magnetic_field"]

    # Downloading first element of hmi magnetogram
    dl = Fido.fetch(hmi_mg[0], path='uploads_fits')

    # Returning array with the fetched image path
    return True, dl


def dpicker_to_sunpy_format(date_from_dt, stop_increment: int) -> str:
    """
    :param date_from_dt: date of the sun acquisition to search
    :param stop_increment: minutes to add on start date for timerange research
    :return: start date
    :return: stop date (from increment in params)
    """
    logging.info(date_from_dt, stop_increment)
    # modif_date_init_format_str = '%a %b %d %H:%M:%S %Y'
    # sunpy_date_format = '%Y-%m-%d %H:%M:%S'

    # create datetime object for timestamp modif date
    start_date = datetime.strptime(date_from_dt, '%Y-%m-%d %H:%M')
    stop_date_increment = start_date + timedelta(minutes=stop_increment)
    stop_date = stop_date_increment.strftime('%Y-%m-%d %H:%M:%S')
    return start_date, stop_date


def get_sdo_img(start_date_for_request: datetime, stop_date_for_request: datetime, language):
    """
    :param start_date_for_request:
    :param stop_date_for_request:
    :param language:
    :return:
    """
    try:
        return _extracted_from_get_sdo_img_5(
            "First try requesting data :",
            start_date_for_request,
            stop_date_for_request,
            language,
        )

    except IndexError as i:
        logging.info(f'No data found for the selected date - Exception {i}: ')
        return 'Index error', False

    except Exception as e:
        logging.info(f' First Error - Error retrieving data {e}')
    try:
        return _extracted_from_get_sdo_img_5(
            "Second try requesting data :",
            start_date_for_request,
            stop_date_for_request,
            language,
        )

    except Exception as e:
        logging.info(f' Second Error - Error retrieving data {e}')
        return 'Error', False


# TODO Rename this here and in `get_sdo_img`
def _extracted_from_get_sdo_img_5(arg0, start_date_for_request, stop_date_for_request, language):
    # First try to fetch data from Sunpy
    logging.info(arg0)
    aia_found, aia_fits = get_aia_img(start_date_for_request, stop_date_for_request)
    hmi_found, hmi_fits = get_hmi_img(start_date_for_request, stop_date_for_request)

    # In the case there is no AIA images found, we consider that there is no data found at all
    if not aia_found:
        # Returning explicit String + DataFound value
        return 'No data found', False

    # Else we merge the fetched data into a single array
    fits_list = aia_fits + hmi_fits

    # Sorting list as the following pattern -> 193 / 304 / 1700 / Magnetogram
    fits_list = sort_fetched_img(fits_list)

    # Saving process for all fetched images + converting as .png format
    path_list = saving_fetched_img(fits_list)

    # In the case the hmi data are missing, we add to the array the default NoDataFound image depending of the page language
    if not hmi_found:
        path_list.append('images/NoDataFoundFR.png') if language == "fr" else path_list.append('images/NoDataFoundEN.png')

    # Return the png path list and data found value of True
    return path_list, True


def saving_fetched_img(img_list: list):

    # Saving process for all fetched images
    path_list = []

    for img in img_list:
        sdo_map = sunpy.map.Map(img)
        plt.figure(figsize=(16, 9))
        plt.subplot(projection=sdo_map)
        sdo_map.plot(clip_interval=(1, 99.99) * u.percent)
        sdo_map.draw_limb()
        sdo_map.draw_grid()

        timestamp_tag = (int(datetime.utcnow().timestamp()))
        file_rename_path = 'static/sunpy_tmp/' + str(timestamp_tag) + '.png'  # <---- Home
        # file_rename_path = 'mysite/static/sunpy_tmp/' + str(timestamp_tag) + '.png'  # <---- online
        # print(file_rename_path)
        # file.save(os.path.join(app.config['UPLOAD_FOLDER'], file_rename))

        plt.savefig(file_rename_path)
        logging.info(f'File saved : {file_rename_path} ')

        # Adding the path to the path list for the return
        path_list.append('sunpy_tmp/' + str(timestamp_tag) + '.png')

    return path_list


def sort_fetched_img(img_list: list):
    aia_193, aia_304, aia_1700, hmi_img = '', '', '', ''

    # Assign founded paths to variables
    for img_ in img_list:
        print()
        if "0193.fits" in img_ or 'aia_lev1_193a' in img_:
            aia_193 = img_
        elif "0304.fits" in img_ or 'aia_lev1_304a' in img_:
            aia_304 = img_
        elif "1700.fits" in img_ or 'aia_lev1_1700a' in img_:
            aia_1700 = img_
        elif "magnetogram.fits" in img_:
            hmi_img = img_

    # Supposed list
    sorted_list = [aia_193, aia_304, aia_1700, hmi_img]

    # Remove elem in list that are empty
    for img in sorted_list:
        if img == '':
            sorted_list.remove(img)

    return sorted_list
