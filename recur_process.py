import os, shutil
import logging
from config import FOLDER_TO_CLEAR
# Cron files


def clean_folders(path_to_clean):
    """
    Remove all the files in the given path

            Parameters:
                    path_to_clean (String): The folder path as a string

    """
    for filename in os.listdir(path_to_clean):
        file_path = os.path.join(path_to_clean, filename)
        print(file_path)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            logging.info('Failed to delete %s. Reason: %s' % (file_path, e))


def clean_all_files():
    """
    Remove all the files from all the folders in the FOLDER_TO_CLEAR constant

    """
    for path in FOLDER_TO_CLEAR:
        clean_folders(path)
    print('Clean done.')


if __name__ == '__main__':
    clean_all_files()
